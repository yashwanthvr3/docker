﻿using System;
using System.Collections.Generic;

namespace XAutomateMVC.Models.DBModels
{
    public partial class ExecutionApiprocess
    {
        public long ExecutionApiprocessId { get; set; }
        public string ExecutionId { get; set; }
        public string Status { get; set; }
        public string CreatedDate { get; set; }
        public string Updatedate { get; set; }
    }
}
