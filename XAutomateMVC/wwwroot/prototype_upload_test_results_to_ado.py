# Setup
import requests
import urllib3
import base64
import json
import re
import os
from robot.api import ExecutionResult, ResultVisitor
from robot.api import ExecutionResult, SuiteVisitor

x =[]
class PrintTestInfo(ResultVisitor):

    def visit_test(self, test):
      #for
      global suitename
      # allday =[]
     # print(test.parent)

      status =test.longname + '.' + test.status
      suitename = status.split('.')[0]
      x.append(status.split('.'))
      #print(x)
      #status =  '{''},{''},{''},{''}'.format(suite,test_appr,test.name,test.status)
      #x.append(status.split(','))

result = ExecutionResult('output.xml')
#result.visit(KeywordMetrics())
result.visit(PrintTestInfo())
#print(x)
#print(suitename)
#print(content)
# Get warnings for https if this is not disabled when connecting to ADO
urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
print(os.environ["tocken"])
# ADO settings
personal_access_token = '4mnjpxgpq4wo3ajfv6hv3hetv6lnjcd5jo4sfiri473naxeoza6q'
organization = 'https://dev.azure.com/x-automate/'
project = 'X-AutoMate'

# Creating headers for api call
username_password = "{}:{}".format('',personal_access_token)
usr_pwd_bytes     = bytes(username_password, 'utf-8')
base64_token = base64.b64encode(usr_pwd_bytes)
#print(base64_token)

headers = {
    'Content-Type': 'application/json',
    'Authorization': f'Basic {base64_token.decode()}'
}


wit_headers = {
    'Content-Type': 'application/json-patch+json',
    'Authorization': f'Basic {base64_token.decode()}'
}


# Test data for development
test_plan_name = suitename

test_data = x


# Helper methods


def get_data(url):
    response = requests.request("GET", url, headers=headers, verify=False)

    return (response.status_code, json.loads(response.content.decode('utf-8')))

def find_object(param,value,objects):
    object_found = False
    for obj in objects:
        if param in obj and obj[param] == value:
            return (True, obj)
    return (False, None)

def create_or_update_object(payload, url, method, headers):
    payload = json.dumps(payload)
    response = requests.request(method, url, headers=headers, data=payload, verify=False)

    return (response.status_code, json.loads(response.content.decode('utf-8')))


# Upload of test results to Azure devops

# Find or create test plan in Azure devops
url = f"{organization}/{project}/_apis/test/plans?api-version=5.0"
status_code, content = get_data(url)
test_plans = content['value']

test_plan_found, test_plan = find_object('name',test_plan_name, test_plans)

if not test_plan_found:
    payload = { 'name': test_plan_name, 'area': { 'name': project }, 'iteration': project}
    url = f"{organization}/{project}/_apis/test/plans?api-version=5.0"

    status_code, content = create_or_update_object(payload, url, "POST", headers)

    test_plan = content['value']


ado_data = {}

# Loop through test results
#print(test_data)
for row in test_data:
    test_suite_name, test_approach_name, test_case_name, test_result = row

    # Find or create test suite in Azure devops
    if  not test_suite_name in ado_data:
        url = f"{organization}/{project}/_apis/test/Plans/{test_plan['id']}/suites?api-version=5.0"
        status_code, content = get_data(url)
        test_suites = content['value']
        test_suite_found, test_suite = find_object('name',test_suite_name, test_suites)

        if not test_suite_found:
            payload = {"suiteType": "StaticTestSuite", "name": test_suite_name}
            url = f"{organization}/{project}/_apis/test/Plans/{test_plan['id']}/suites/{test_plan['rootSuite']['id']}?api-version=5.0"

            status_code, content = create_or_update_object(payload, url, "POST", headers)
            test_suite = content['value'][0]

        ado_data[test_suite_name] = {'id': test_suite['id']}

    # Find or create test approach in Azure devops
    if  not test_approach_name in ado_data[test_suite_name]:
        if not 'test_suites' in locals():
            url = f"{organization}/{project}/_apis/test/Plans/{test_plan['id']}/suites?api-version=5.0"
            status_code, content = get_data(url)
            test_suites = content['value']

        test_approach_found, test_approach = find_object('name',test_approach_name, test_suites)

        if not test_approach_found:
            payload = {"suiteType": "StaticTestSuite", "name": test_approach_name}
            url = f"{organization}/{project}/_apis/test/Plans/{test_plan['id']}/suites/{ado_data[test_suite_name]['id']}?api-version=5.0"

            status_code, content = create_or_update_object(payload, url, "POST", headers)
            test_approach = content['value'][0]

        ado_data[test_suite_name][test_approach_name] = {'id': test_approach['id']}

    # Find or create test case in Azure devops
    if not test_case_name in ado_data[test_suite_name][test_approach_name]:
        url = f"{organization}/{project}/_apis/test/Plans/{test_plan['id']}/suites/{ado_data[test_suite_name][test_approach_name]['id']}/testcases?api-version=5.0"
        status_code, content = get_data(url)
        test_case_ids = [x['testCase']['id'] for x in content['value']]

        if test_case_ids:
            url = f"{organization}/{project}/_apis/wit/workitems?ids={','.join(test_case_ids)}&api-version=5.0"
            status_code, content = get_data(url)
            test_cases = [{'id': x['id'], 'name': x['fields']['System.Title']} for x in content['value']]

            test_case_found, test_case = find_object('name',test_case_name, test_cases)
        else:
            test_case_found = False

        if not test_case_found:
            payload = [{"op": "add", "path": "/fields/System.Title", "from": None, "value": test_case_name}]
            url = f"{organization}/{project}/_apis/wit/workitems/$Test Case?api-version=5.0"
            status_code, work_item = create_or_update_object(payload, url, "POST", wit_headers)

            url = f"{organization}/{project}/_apis/test/Plans/{test_plan['id']}/suites/{ado_data[test_suite_name][test_approach_name]['id']}/testcases/{work_item['id']}?api-version=5.0"
            status_code, content = create_or_update_object('', url, "POST", headers)
            test_case = content['value'][0]['testCase']

        ado_data[test_suite_name][test_approach_name][test_case_name] = {'id': test_case['id']}

    # Get points to test cases in Azure devops
    url = f"{organization}/{project}/_apis/test/Plans/{test_plan['id']}/Suites/{ado_data[test_suite_name][test_approach_name]['id']}/points?api-version=5.0"
    status_code, content = get_data(url)
    points = [{'id': x['id'], 'name': x['testCase']['name'], 'test_case_id': x['testCase']['id']} for x in content['value']]
    point_found, point = find_object('name',test_case_name, points)

    if point_found:
        if test_result == "PASS":
            test_result_value = "Passed"
        else:
            test_result_value = "Failed"

    points_with_results[point['id']] = test_result_value

# Exit loop of data
# Create Test Run
url = f"{organization}/{project}/_apis/test/runs?api-version=5.0"
points_in_run = list(points_with_results.keys())
payload = {"name": tag, "plan": {"id": test_plan['id']}, "pointIds": points_in_run}
status_code, test_run = create_or_update_object(payload, url, "POST", headers)

# Add Results objects to Test Run
url = f"{organization}/{project}/_apis/test/Runs/{test_run['id']}/results?api-version=5.0"
status_code, content = get_data(url)
test_results = content['value']

# Update Test Results with actual results
url = f"{organization}/{project}/_apis/test/Runs/{test_run['id']}/results?api-version=5.0"
payload = [{"id": t['id'], "outcome": points_with_results[int(t['testPoint']['id'])], "state": "Completed"} for t in test_results]
status_code, content = create_or_update_object(payload, url, "PATCH", headers)
