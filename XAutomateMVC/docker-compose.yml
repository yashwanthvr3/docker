## You can generate a custom docker compose file automatically on http://reportportal.io/download (Step 2)

## This is example of Docker Compose for ReportPortal
## Do not forget to configure data volumes for production usage

## Execute 'docker-compose -f docker-compose.yml -p reportportal up -d --force-recreate' --build
## to start all containers in daemon mode
## Where:
##      '-f docker-compose.yml' -- specifies this compose file
##      '-p reportportal' -- specifies container's prefix (project name)
##      '-d' -- enables daemon mode
##      '--force-recreate' -- forces re-recreating of all containers

version: '2.4'
services:

  gateway:
    image: traefik:v2.0.5
    ports:
      - "8080:8080" # HTTP exposed
      - "8081:8081" # HTTP Administration exposed
      - "80:80"
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
    command:
      - --providers.docker=true
      - --providers.docker.constraints=Label(`traefik.expose`, `true`)
      - --entrypoints.web.address=:8080
      - --entrypoints.webui.address=:80
      - --entrypoints.traefik.address=:8081
      - --api.dashboard=true
      - --api.insecure=true
    restart: always

  elasticsearch:
    image: docker.elastic.co/elasticsearch/elasticsearch:7.3.0
    volumes:
      - ./data/elasticsearch:/usr/share/elasticsearch/data
    environment:
      - "bootstrap.memory_lock=true"
      - "discovery.type=single-node"
      - "logger.level=INFO"
      - "xpack.security.enabled=true"
      - "ELASTIC_PASSWORD=elastic1q2w3e"
    ulimits:
      memlock:
        soft: -1
        hard: -1
      nofile:
        soft: 65536
        hard: 65536
      # ports:
      # - "9200:9200"
    healthcheck:
      test: ["CMD", "curl","-s" ,"-f", "http://elastic:elastic1q2w3e@localhost:9200/_cat/health"]
    restart: always

  analyzer:
    image: reportportal/service-auto-analyzer:5.3.5
    environment:
      LOGGING_LEVEL: info
      AMQP_URL: amqp://rabbitmq:rabbitmq@rabbitmq:5672
      ES_HOSTS: http://elastic:elastic1q2w3e@elasticsearch:9200
      MINIO_SHORT_HOST: minio:9000
      MINIO_ACCESS_KEY: minio
      MINIO_SECRET_KEY: minio123
    depends_on:
      - elasticsearch
    restart: always

  ### Initial reportportal db schema. Run once.
  db-scripts:
    image: reportportal/migrations:5.3.5
    depends_on:
      postgres:
        condition: service_healthy
    environment:
      POSTGRES_SERVER: postgres
      POSTGRES_PORT: 5432
      POSTGRES_DB: reportportal
      POSTGRES_USER: rpuser
      POSTGRES_PASSWORD: rppass
    restart: on-failure

  postgres:
    image: postgres:12-alpine
    shm_size: '512m'
    environment:
      POSTGRES_USER: rpuser
      POSTGRES_PASSWORD: rppass
      POSTGRES_DB: reportportal
    volumes:
      # For unix host
      - ./data/postgres:/var/lib/postgresql/data
      # For windows host
      # - postgres:/var/lib/postgresql/data
    # If you need to access the DB locally. Could be a security risk to expose DB.
    #    ports:
    #      - "5432:5432"
    command:
      -c checkpoint_completion_target=0.9
      -c work_mem=96MB
      -c wal_writer_delay=20ms
      -c synchronous_commit=off
      -c wal_buffers=32MB
      -c min_wal_size=2GB
      -c max_wal_size=4GB
    # Optional, for SSD Data Storage. If you are using the HDD, set up this command to '2'
    #  -c effective_io_concurrency=200
    # Optional, for SSD Data Storage. If you are using the HDD, set up this command to '4'
    #  -c random_page_cost=1.1
    # Optional, can be scaled. Example for 4 CPU, 16GB RAM instance, where only the database is deployed
    #  -c max_worker_processes=4
    #  -c max_parallel_workers_per_gather=2
    #  -c max_parallel_workers=4
    #  -c shared_buffers=4GB
    #  -c effective_cache_size=12GB
    #  -c maintenance_work_mem=1GB
    healthcheck:
      test: ["CMD-SHELL", "pg_isready -d $$POSTGRES_DB -U $$POSTGRES_USER"]
      interval: 10s
      timeout: 120s
      retries: 10
    restart: always

  rabbitmq:
    image: rabbitmq:3.7.16-management
    #ports:
      # - "5672:5672"
      #- "15672:15672"
    environment:
      RABBITMQ_DEFAULT_USER: "rabbitmq"
      RABBITMQ_DEFAULT_PASS: "rabbitmq"
    healthcheck:
      test: ["CMD", "rabbitmqctl", "status"]
      retries: 5
    restart: always

  uat:
    image: reportportal/service-authorization:5.3.5
    #ports:
    #  - "9999:9999"
    environment:
      - RP_DB_HOST=postgres
      - RP_DB_USER=rpuser
      - RP_DB_PASS=rppass
      - RP_DB_NAME=reportportal
      - RP_BINARYSTORE_TYPE=minio
      - RP_BINARYSTORE_MINIO_ENDPOINT=http://minio:9000
      - RP_BINARYSTORE_MINIO_ACCESSKEY=minio
      - RP_BINARYSTORE_MINIO_SECRETKEY=minio123
      - RP_SESSION_LIVE=86400 #in seconds
    labels:
      - "traefik.http.middlewares.uat-strip-prefix.stripprefix.prefixes=/uat"
      - "traefik.http.routers.uat.middlewares=uat-strip-prefix@docker"
      - "traefik.http.routers.uat.rule=Host(`xautomate.se`) && PathPrefix(`/uat`)"
      - "traefik.http.routers.uat.service=uat"
      - "traefik.http.services.uat.loadbalancer.server.port=9999"
      - "traefik.http.services.uat.loadbalancer.server.scheme=http"
      - "traefik.expose=true"
    restart: always

  index:
    image: reportportal/service-index:5.0.10
    depends_on:
      gateway:
        condition: service_started
    environment:
      - LB_URL=http://gateway:8081
      - TRAEFIK_V2_MODE=true
    labels:
        - "traefik.http.routers.index.rule=PathPrefix(`/`)"
        - "traefik.http.routers.index.service=index"
        - "traefik.http.services.index.loadbalancer.server.port=8080"
        - "traefik.http.services.index.loadbalancer.server.scheme=http"
        - "traefik.expose=true"
    restart: always
  apitest:
    image: reportportal/service-api:5.3.5
    depends_on:
      rabbitmq:
        condition: service_healthy
      gateway:
        condition: service_started
      postgres:
        condition: service_healthy
    environment:
      - RP_DB_HOST=postgres
      - RP_DB_USER=rpuser
      - RP_DB_PASS=rppass
      - RP_DB_NAME=reportportal
      - RP_AMQP_USER=rabbitmq
      - RP_AMQP_PASS=rabbitmq
      - RP_AMQP_APIUSER=rabbitmq
      - RP_AMQP_APIPASS=rabbitmq
      - RP_BINARYSTORE_TYPE=minio
      - RP_BINARYSTORE_MINIO_ENDPOINT=http://minio:9000
      - RP_BINARYSTORE_MINIO_ACCESSKEY=minio
      - RP_BINARYSTORE_MINIO_SECRETKEY=minio123
      - LOGGING_LEVEL_ORG_HIBERNATE_SQL=info
      - JAVA_OPTS=-Xmx1g -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp  -Dcom.sun.management.jmxremote.rmi.port=12349 -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.local.only=false  -Dcom.sun.management.jmxremote.port=9010 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=0.0.0.0
    labels:
      - "traefik.http.middlewares.apitest-strip-prefix.stripprefix.prefixes=/api"
      - "traefik.http.routers.apitest.middlewares=api-strip-prefix@docker"
      - "traefik.http.routers.apitest.rule=Host(`xautomate.se`) && PathPrefix(`/api`)"
      - "traefik.http.routers.apitest.service=api"
      - "traefik.http.services.apitest.loadbalancer.server.port=8585"
      - "traefik.http.services.apitest.loadbalancer.server.scheme=http"
      - "traefik.expose=true"
    restart: always

  api:
    image: reportportal/service-api:5.3.5
    depends_on:
      rabbitmq:
        condition: service_healthy
      gateway:
        condition: service_started
      postgres:
        condition: service_healthy
    environment:
      - RP_DB_HOST=postgres
      - RP_DB_USER=rpuser
      - RP_DB_PASS=rppass
      - RP_DB_NAME=reportportal
      - RP_AMQP_USER=rabbitmq
      - RP_AMQP_PASS=rabbitmq
      - RP_AMQP_APIUSER=rabbitmq
      - RP_AMQP_APIPASS=rabbitmq
      - RP_BINARYSTORE_TYPE=minio
      - RP_BINARYSTORE_MINIO_ENDPOINT=http://minio:9000
      - RP_BINARYSTORE_MINIO_ACCESSKEY=minio
      - RP_BINARYSTORE_MINIO_SECRETKEY=minio123
      - LOGGING_LEVEL_ORG_HIBERNATE_SQL=info
      - JAVA_OPTS=-Xmx1g -XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/tmp  -Dcom.sun.management.jmxremote.rmi.port=12349 -Dcom.sun.management.jmxremote -Dcom.sun.management.jmxremote.local.only=false  -Dcom.sun.management.jmxremote.port=9010 -Dcom.sun.management.jmxremote.authenticate=false -Dcom.sun.management.jmxremote.ssl=false -Djava.rmi.server.hostname=0.0.0.0
    labels:
      - "traefik.http.middlewares.api-strip-prefix.stripprefix.prefixes=/api"
      - "traefik.http.routers.api.middlewares=api-strip-prefix@docker"
      - "traefik.http.routers.api.rule=PathPrefix(`/api`)"
      - "traefik.http.routers.api.service=api"
      - "traefik.http.services.api.loadbalancer.server.port=8585"
      - "traefik.http.services.api.loadbalancer.server.scheme=http"
      - "traefik.expose=true"
    restart: always
  ui:
    image: reportportal/service-ui:5.3.5
    environment:
      - RP_SERVER_PORT=8080
      - RP_SERVER_ROOT_URL=%(protocol)s://%(domain)s:%(http_port)s/reportportal
    labels:
      - "traefik.http.routers.ui.rule=Host(`xautomate.se`) && PathPrefix(`/reportportal`)"
            #    - "traefik.http.middlewares.ui-strip-prefix.stripprefix.prefixes=/ui"
            # - "traefik.http.routers.ui.middlewares=ui-stripprefix@docker"
        # - "traefik.http.routers.ui.tls=true"
      - "traefik.http.middlewares.test-redirectregex.redirectregex.regex=^(https|http)://([a-zA-Z0-9_.-]+)/([a-zA-Z0-9_.-]+)$$"
      - "traefik.http.middlewares.test-redirectregex.redirectregex.replacement=$${1}://$${2}/$${3}/"
      - "traefik.http.middlewares.ui-stripprefix.stripprefix.prefixes=/reportportal,/login" 
        #  - "traefik.http.routers.ui.rule=Host(`xautomate.com`) && PathPrefix(`/reportportal`)"
        # - "traefik.http.routers.ui.service=ui"
      - "traefik.http.services.ui.loadbalancer.server.port=8080"
      - "traefik.http.services.ui.loadbalancer.server.scheme=http"
        # - "traefik.enable=true"
        #- "traefik.http.routers.ui.entrypoints=web"
      - "traefik.http.routers.ui.middlewares=test-redirectregex@docker,ui-stripprefix@docker,ui-stripprefix"
      - "traefik.expose=true"
    restart: always

  minio:
    image: minio/minio:RELEASE.2020-10-27T04-03-55Z
    #ports:
    #  - '9000:9000'
    volumes:
     # For unix host
      - ./data/storage:/data 
     # For windows host
     # - minio:/data
    environment:
      MINIO_ACCESS_KEY: minio
      MINIO_SECRET_KEY: minio123
    command: server /data
    healthcheck:
      test: ["CMD", "curl", "-f", "http://localhost:9000/minio/health/live"]
      interval: 30s
      timeout: 20s
      retries: 3
    restart: always

  # Docker volume for Windows host
#volumes:
#  postgres:
#  minio:
  backend:
    image: newtestcont
    ports:
      - "5005:80"
    volumes:
      - /var/backend:/srv/www/XAutomate/wwwroot/Result
    environment:
      - ReportPortalIp=${ReportPortalIp}
      - ReportPortalGUIURL=${ReportPortalGUIURL}
      - graphanaIp=${graphanaIp}
      - graphanadbIp=${graphanadbIp}
      - graphanadbPort=${graphanadbPort}
      - graphanausername=${graphanausername}
      - graphanapass=${graphanapass}
      - graphanaDbname=${graphanaDbname}
      - ReportPortalUid=${ReportPortalUid}
      - MysqlConnectionString=${MysqlConnectionString}
      - apiurl=${apiurl}
      - StylesheetUrl=http://xautomate.com/backend    
    labels:
            # - "traefik.enable=true"
      - "traefik.http.routers.backend.rule=Host(`xautomate.se`) && PathPrefix(`/backend`)"
      - "traefik.http.middlewares.backend-stripprefix.stripprefix.prefixes=/backend,/"
        # - "traefik.http.routers.backend.service=/"
      - "traefik.http.routers.backend.middlewares=backend-stripprefix"
      - "traefik.http.routers.backend.entrypoints=webui"
      - "traefik.expose=true"
    restart: always
  grafanaweb:
    build: .
    image: grafana/grafana:6.5.0
    ports:
      - "3300:3000"
    volumes:
      - /var/grafana-storage:/var/lib/grafana     
    environment:
      - GF_AUTH_ANONYMOUS_ENABLED=true
      - GF_SECURITY_ALLOW_EMBEDDING=true
      - GF_SERVER_ROOT_URL=%(protocol)s://%(domain)s:%(http_port)s/grafana
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.grafanaweb.rule=Host(`xautomate.se`) && PathPrefix(`/grafana`)"
      - "traefik.http.middlewares.grafanaweb-stripprefix.stripprefix.prefixes=/grafana,/login"   
      - "traefik.http.routers.grafanaweb.entrypoints=webui"
      - "traefik.http.routers.grafanaweb.middlewares=grafanaweb-stripprefix"
      - "traefik.expose=true"
    restart: always
  frontend:
    build: .
    image: guicontainer
    ports:
      - "5001:80"
    volumes:
      - /var/frontend:/srv/www/XAutomate/File
    environment:
      - ReportPortalIp=${ReportPortalIp}
      - ReportPortalGUIURL=${ReportPortalGUIURL}
      - graphanaIp=${graphanaIp}
      - MysqlConnectionString=${MysqlConnectionString}
      - apiurl=${apiurl}
      - StylesheetUrl=http://xautomate.com/frontend
        # - F_SERVER_ROOT_URL=%(protocol)s://%(domain)s:%(http_port)s/backend
    labels:
      - "traefik.enable=true"
        # - "traefik.http.routers.frontend.rule=Host(`xautomate.frontend.xautomate.com`)"
      - "traefik.http.routers.frontend.rule=Host(`xautomate.se`)" 
      - "traefik.http.middlewares.test-redirectregex.redirectregex.regex=^(https|http)://([a-zA-Z0-9_.-]+)/([a-zA-Z0-9_.-]+)$$"
      - "traefik.http.middlewares.test-redirectregex.redirectregex.replacement=$${1}://$${2}/$${3}/"
        #    - "traefik.http.middlewares.frontend-stripprefix.stripprefix.prefixes=/frontend"
        #    - "traefik.http.routers.frontend.service=/"
      - "traefik.http.routers.frontend.entrypoints=webui"
        #: - "traefik.http.routers.frontend.middlewares=test-redirectregex@docker,frontend-stripprefix"
      - "traefik.expose=true"
      - "traefik.http.routers.frontend_http.priority=1"
    restart: always   
  Internaldatabase:
    build: .
    image: mysql/mysql-server:8.0
    volumes:
      - /opt/mysql_data/internal:/var/lib/mysql
    ports:
      - "3306:3306"
    command: --default-authentication-plugin=mysql_native_password
    restart: always
    environment:
       MYSQL_ROOT_PASSWORD: password

  grafanadatabase:
    build: .
    image: mysql/mysql-server:5.7
    volumes:
      - /opt/mysql_data/grafana:/var/lib/mysql
    ports:
      - "3307:3306"
    command: --default-authentication-plugin=mysql_native_password
    restart: always
    environment:
       MYSQL_ROOT_PASSWORD: password

